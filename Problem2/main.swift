//
//  main.swift
//  Problem2
//
//  Created by admin on 04.10.16.
//  Copyright © 2016 OneTwoTrip. All rights reserved.
//

import Foundation

if let nodes = InputOutput.matrixOfNodes() {
    var result = Solver.resultArrayWithNodes(nodes)
    InputOutput.printResult(result)
} else {
    print("No input file")
}