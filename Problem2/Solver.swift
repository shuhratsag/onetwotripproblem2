//
//  Solver.swift
//  Problem2
//
//  Created by admin on 04.10.16.
//  Copyright © 2016 OneTwoTrip. All rights reserved.
//

import Foundation

class Solver {
    
    static func resultArrayWithNodes(nodes: [[Node]]) -> [Int] {
        let n = nodes.count / 2
        if n > 0 {
            let startNode = nodes[n][n]
            startNode.direction = .left
            var result = [Int]()
            recursiveSearch(startNode, nodes: nodes, result: &result)
            return result
        } else {
            return [Int]()
        }
    }
    
    private static func recursiveSearch(node: Node, nodes: [[Node]], inout result: [Int]) {
        node.visited = true
        if let nextNode = nextNode(node, nodes: nodes) {
            node.direction?.incrementDirection()
            nextNode.direction = node.direction
            result.append(node.number)
            recursiveSearch(nextNode, nodes: nodes, result: &result)
        } else {
            if node.i == 0 && node.j == 0 {
                result.append(node.number)
                return
            }
            node.direction?.decrementDirection()
            recursiveSearch(node, nodes: nodes, result: &result)
        }
    }
    
    private static func nextNode(node: Node, nodes: [[Node]]) -> Node? {
        let (x, y) = node.nextIndexes()
        if nodes.indices.contains(x) && nodes.indices.contains(y) && nodes[x][y].visited == false {
            return nodes[x][y];
        }
        return nil;
    }
}