//
//  InputOutput.swift
//  Problem2
//
//  Created by admin on 04.10.16.
//  Copyright © 2016 OneTwoTrip. All rights reserved.
//

import Foundation

class InputOutput {
    
    static func matrixOfNodes() -> [[Node]]? {
        if let inputString = try? String(contentsOfFile: "input.txt", encoding: NSUTF8StringEncoding) {
            var stringArr = inputString.componentsSeparatedByCharactersInSet((NSCharacterSet.whitespaceAndNewlineCharacterSet()))
            stringArr = stringArr.filter({ (string) -> Bool in
                if string != "" {
                    return true
                }
                return false
            })
            let n = Int(stringArr.first!)!
            stringArr.removeFirst()
            var nodes = [[Node]]()
            for (var i = 0;  i < (2 * n - 1); ++i) {
                var row = [Node]()
                for (var j = 0; j < (2 * n - 1); ++j) {
                    row.append(Node(number: Int(stringArr[i * (2 * n - 1) + j])!, i: i, j: j))
                }
                nodes.append(row)
            }
            return nodes
        }
        return nil;
    }
    
    static func printResult(result: [Int]) {
        var printStr = ""
        for number in result {
            let str = String(format: "%d ", number)
            printStr.appendContentsOf(str)
        }
        try? printStr.writeToFile("output.txt", atomically: true, encoding: NSUTF8StringEncoding)
    }
}