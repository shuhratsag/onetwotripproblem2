//
//  Node.swift
//  Problem2
//
//  Created by admin on 04.10.16.
//  Copyright © 2016 OneTwoTrip. All rights reserved.
//

import Foundation

enum Direction: Int {
    case left
    case down
    case right
    case up
    mutating func incrementDirection() {
        if self == .up {
            self = .left
        } else {
            self = Direction(rawValue: self.rawValue + 1)!
        }
    }
    mutating func decrementDirection() {
        if self == .left {
            self = .up
        } else {
            self = Direction(rawValue: self.rawValue - 1)!
        }
    }
}

class Node {
    
    var number: Int
    var i: Int;
    var j: Int;
    var direction: Direction?
    var visited = false
    
    init(number: Int, i: Int, j: Int) {
        self.number = number
        self.i = i;
        self.j = j;
    }
    
    func nextIndexes() -> (Int, Int) {
            switch direction! {
            case .left:
                return (i, j - 1)
            case .down:
                return (i + 1, j)
            case .right:
                return (i, j + 1)
            case .up:
                return (i - 1, j)
            }
    }
    
}